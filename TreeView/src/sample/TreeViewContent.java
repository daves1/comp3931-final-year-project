package sample;

import javafx.scene.control.TreeItem;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The TreeViewContent class reads in the stored content of the tree and dynamically creates a tree representation.
 *
 * @author Dan Aves
 */


public class TreeViewContent {

    //default constructor
    public TreeViewContent() {
    }

        int branchLevel, indexCountBefore, totalIndex, newParent = 0;

    /**
     * This method constructs the tree representation of each criteria item
     * @return an array list containing each TreeItem from the criteria.
     * @param filename Supplied datafile name to retrieve input from
     */
    public ArrayList<TreeItem> getCriteria(String filename) {
            //Populate scanner object from returned scanner
            Scanner sc = null;
            try{
                sc = ReadInput(filename);
            }catch  (FileNotFoundException ex){
                System.out.println("File cannot be found");
            }

            //Final list to contain the parent of each individual branch
            ArrayList<TreeItem> criteria = new ArrayList<TreeItem>();

            //List to store point on tree where new branch is started
            List<Integer> branchList = new ArrayList<>();

            //Stores all the treeview items
            List<TreeItem> list = new ArrayList<>();

            //Read contents of tree from text file until end
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                String item = line.trim();
                //Number of tabs from margin
                int indexCount = CountIndex(line);

                TreeItem currentTreeItem = new TreeItem(item);
                if (indexCount == 0) {
                    //Add total index the current branch starts at
                    branchList.add(totalIndex);
                    //Top of branch (parent) starts with itself
                    currentTreeItem = makeBranch(item, currentTreeItem);
                    list.add(currentTreeItem);
                    //Reset level of tree
                    branchLevel = 0;
                }
                //Checks if current item is different index on the tree, instead of a subcategory of the previous parent
                else if (indexCount < indexCountBefore) {
                    //Tree level dictates current location on a particular branch
                    int treeIndex = (totalIndex - branchLevel) + indexCount;
                    //Make branch here
                    currentTreeItem = makeBranch(item, list.get(treeIndex - 1));
                    //Update parent reference
                    newParent = treeIndex-1;

                    list.add(currentTreeItem);
                }
                //If the current item shares the same parent as the item before
                else if (indexCount == indexCountBefore) {
                    currentTreeItem = makeBranch(item, list.get(newParent));
                    list.add(currentTreeItem);
                }
                //If the current item is a subcategory/child of the previous entry
                else {
                    currentTreeItem = makeBranch(item, list.get(totalIndex - 1));
                    list.add(currentTreeItem);
                    newParent = totalIndex -1;

                }
                branchLevel ++;
                totalIndex++;
                indexCountBefore = indexCount;

            }
            //Add each tree at the beginning of their branch
            for(int i = 0; i<branchList.size(); i++){
                criteria.add(list.get(branchList.get(i)));
            }

            return criteria;
        }

    /**
     * This method constructs the branch for the tree
     * @return TreeItem of the new branch
     * @param title Title of new branch
     * @param parent parent to create branch from
     */
    public TreeItem<String> makeBranch(String title, TreeItem<String> parent) {
        TreeItem<String> item = new TreeItem<>(title);
        item.setExpanded(true);
        parent.getChildren().add(item);
        return item;
    }


    /**
     * This method reads the input from a tab delimited text
     * @return Scanner of the file
     * @param fileName for datafile input
     * @throws FileNotFoundException to handle no such file
     */
    public Scanner ReadInput(String fileName) throws FileNotFoundException{
        File file = new File(fileName);
        Scanner sc = null;
        sc = new Scanner(file);

        return sc;
    }

    /**
     * This method counts the number of tabs until the criteria. The amount of tabs indicates the index on the tre.
     * @return count of tabs - 'index'
     * @param line from text file
     */
    public int CountIndex(String line){
        int count = 0;
        for (int i = 0; i < line.length(); i++){
            if (line.charAt(i) == '\t'){
                count++;
            }
        }
        return count;
    }
}
