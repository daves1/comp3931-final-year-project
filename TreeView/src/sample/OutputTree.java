package sample;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The OutputTree class outputs the current tree state to a text file
 *
 * @author Dan Aves
 */
public class OutputTree {
    public boolean outputTree(LinkedHashMap<String,Integer> treeIndex){

        boolean success;

        try{
            FileWriter handler = new FileWriter("criteria.txt", false);

            PrintWriter pw = new PrintWriter(handler);

            Iterator it = treeIndex.entrySet().iterator();
            while (it.hasNext()){
                Map.Entry pair = (Map.Entry)it.next();
                String result = "";
                //Append tabs for index
                for(int i =0; i < (int) pair.getValue(); i++){
                    result += "\t";
                }
                result += pair.getKey().toString();
                pw.println(result);
            }
            pw.close();
            success = true;

        }catch (IOException ex){
            success = false;
            System.out.println(ex);
        }

    return success;
    }
}
