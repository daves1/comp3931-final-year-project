import java.util.ArrayList;

/**
 * This class represents the Geography table in the timetable planning rules
 */
public class Geography extends CommonMethods {

    /**
     * Unique ID for particular Geography instance
     */
    private int geographyID;
    /**
     * Timing Point in Geography
     */
    private String timingPoint;
    /**
     * Direction train is travelling - down
     */
    private String down;
    /**
     * Direction train is travelling - up
     */
    private String up;
    /**
     * Code to flag further information
     */
    private String code;
    /**
     * Specific rule type
     */
    private String ruleType; //Headway, etc
    /**
     * Stores any further information
     */
    private String notes;

    private static ArrayList<Geography> geographyList = new ArrayList<Geography>();

    public Geography() {
    };

    public Geography(int geographyID, String routeCode, String regionName, String timingPoint, String down, String up, String code, String ruleType, String notes){
        this.geographyID = geographyID;
        this.routeCode = routeCode;
        this.regionName = regionName;
        this.timingPoint = timingPoint;
        this.down = down;
        this.up = up;
        this.code = ruleType;
        this.ruleType = ruleType;
        this.notes = notes;
    }

    public int getGeographyID() {
        return geographyID;
    }

    public String getTimingPoint() {
        return timingPoint;
    }

    public String getDown() {
        return down;
    }

    public String getUp() {
        return up;
    }

    public String getCode() {
        return code;
    }

    public String getRuleType() {
        return ruleType;
    }

    public String getNotes() {
        return notes;
    }

    public ArrayList<Geography> getGeographyList() {
        return geographyList;
    }

    public static void setGeographyList(ArrayList<Geography> geographyList) {
        Geography.geographyList = geographyList;
    }

}
