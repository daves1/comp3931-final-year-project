@author Dan Aves

This repository contains two directories: 
/TPRRun - Holds the Timetable Planning Rules Testing application
/TreeView - Holds the Criteria rule editing application 

rules.db is the database for the rule testing application 

Requirements:

	- Apache Ant or Intellij 
	- Java 8.0 
	- JavaFx 8.0 

Compilation and running: 

	Navigate into one of the directories. 

	-Type 'ant run' 

	Doc Comments: 

	-Type 'ant docs' 

	Unit Tests: 
	-Type 'ant test' 

Operating Instructions: 

	Rule Tester:

	Upon running the application, you will be presented with a graphical user interface. Here the user can specify various assumptions for both Train 1 and Train 2. 
	The fields Route Code, Region and Direction must be populated. 
	In the projects current state only Route Code ‘LN600’, Region ‘London North Eastern’ and Direction ‘Up’ or ‘Down’ can be entered. All other options in the interface are optional, these can consist of: 
		- Service Type (Type of train service, i.e class1, stopper, nonstop) 
		- Line Code (Line code from the TPR, i.e LSL for Leeds line) 
		- Train Stop At (Any permitted station the train might be stopping at) 
		- Train not stop at (Any permitted station the train won't be stopping at) 

	Once the desired assumptions are entered clicking ‘Run’ will perform the tests and output the results to the area on the right of the interface. 

	Results for the test can be viewed on the interface and found in the file ‘rulesApplied.txt’. 


Rule Criteria editor:

	Once the application is running you will be presented with a graphical interface. The tree presented can be modified in the following ways:
	1.	Add Node button – Click on any node in the tree that you wish to be the parent. Enter your desired text for the node in the textbox and click ‘Add node’. 
		You should now see the node added to the tree under the parent. 
	2.	Remove Node button – Click on any node in the tree that you wish to remove. Clicking ‘Remove Node’ will delete the selected node from the tree. 
	3. 	Modify Node - Click on the node, it can now be edited. 
	4. 	Save Changes button - Will save the current trees state to the underlying text file criteria.txt 




