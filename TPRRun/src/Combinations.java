import java.util.ArrayList;
import java.util.List;

/**
 * The Combinations class is used to generate every possible assumption for multi-case automated testing.
 *
 * @author Dan Aves
 */
public class Combinations {

    /**
     * Method to generate each assumption
     * @return Arraylist containing a list of Assumptions.class representations
     */
    public ArrayList<Assumptions> assumptionCombinations(){
        ArrayList<Assumptions> assumptionsList = new ArrayList<>();

        //Create arraylist for possible assumptions to consider
        List<String> direction = new ArrayList<>();
        direction.add("UP");
        direction.add("DOWN");

        List<String> following = new ArrayList<>();
        following.add("NONSTOP");
        following.add("STOPPER");
        following.add("CLASS1");

        List<String> serviceType = new ArrayList<>();
        serviceType.add("NONSTOP");
        serviceType.add("STOPPER");
        serviceType.add("CLASS1");

        List<String> lineCode = new ArrayList<>();
        lineCode.add("FL");
        lineCode.add("SL");
        lineCode.add("LSL");

        List<String> routeCode = new ArrayList<>();
        routeCode.add("routeCode");

        //Loop through each array list
        for (int directionCount = 0; directionCount<direction.size(); directionCount++){
            for (int followingCount = 0; followingCount<following.size(); followingCount++) {
                for (int serviceTCount = 0; serviceTCount < serviceType.size(); serviceTCount++) {
                    for (int lcCount = 0; lcCount < lineCode.size(); lcCount++) {

                            //Create assumption object based on current index
                            Assumptions assumptions = new Assumptions("LN600", "London North Eastern", direction.get(directionCount), following.get(followingCount), serviceType.get(serviceTCount), lineCode.get(lcCount), "York", null);
                            //Store a list of each assumption object generated
                            assumptionsList.add(assumptions);
                    }
                }
            }
        }
        return assumptionsList;
    }
}
