import java.sql.*;
import java.util.ArrayList;

/**
 * Class used to query rules.db to extract the relevant information
 */
public class FetchData {

    private static PreparedStatement geographyStatement = null;
    private static PreparedStatement headwayStatement = null;
    private static String queryParam1 = null;
    private static String queryParam2 = null;


    /**
     * Responsilbe for executing the respective database queries. Results are appended to Geography and Headway collections.
     * @param routeCode entered from interface
     * @param regionName entered from interface
     * @throws SQLException Handles potential SQL error
     */
    public static void createCollection(String routeCode, String regionName) throws SQLException{

        ArrayList<Headway> headwayCol = new ArrayList<Headway>();
        ArrayList<Geography> geographyCol = new ArrayList<Geography>();

        createQueries(routeCode,regionName);

        ResultSet rsG = geographyStatement.executeQuery();
        while(rsG.next()){
            Geography geography = new Geography(rsG.getInt("GeographyID"),
                    rsG.getString("RouteCode"),
                    rsG.getString("RegionName"),
                    rsG.getString("TimingPoint"),
                    rsG.getString("Down"),
                    rsG.getString("Up"),
                    rsG.getString("Code"),
                    rsG.getString("RuleType"),
                    rsG.getString("Notes"));
            geographyCol.add(geography);
        }
        
        ResultSet rsH = headwayStatement.executeQuery();
        while(rsH.next()) {
            Headway headway = new Headway(rsH.getInt("RuleId"),
                    rsH.getString("RouteCode"),
                    rsH.getString("RegionName"),
                    rsH.getString("TimingPointFrom"),
                    rsH.getString("TimingPointTo"),
                    rsH.getString("Direction"),
                    rsH.getString("Following"),
                    rsH.getString("ServiceType"),
                    rsH.getString("LineCode"),
                    rsH.getString("StopAt"),
                    rsH.getString("NotStopAt"),
                    rsH.getInt("Value"),
                    rsH.getString("Standard"));
            headwayCol.add(headway);


        }

        Headway headway = new Headway();
        headway.setHeadwayList(headwayCol);
        Geography geography = new Geography();
        geography.setGeographyList(geographyCol);

    }


    /**
     * This method creates the respective database queries using prepared statements
     * @param routeCode from user input, required for query
     * @param regionName from user input, required for query
     */
    public static void createQueries(String routeCode, String regionName){
        try {
            DbConnection db = new DbConnection();
            Connection connection = db.openConnection();


            String geographyQuery = "SELECT * FROM Geography WHERE RouteCode=? AND RegionName=?";
            String query = String.format(geographyQuery);
            geographyStatement = connection.prepareStatement(query);
            geographyStatement.setString(1, routeCode);
            geographyStatement.setString(2, regionName);

            //for unit testing
            queryParam1 = routeCode;
            queryParam2 = regionName;

            String headwayQuery = "SELECT * FROM Headway WHERE RouteCode=? AND RegionName=?";
            String query2 = String.format(headwayQuery);
            headwayStatement = connection.prepareStatement(query2);
            headwayStatement.setString(1, routeCode);
            headwayStatement.setString(2, regionName);

        }catch(SQLException e){
            System.err.println("Generating prepared statements failed");
            System.err.println(e.getMessage());

        }
    }

    /**
     * Public getter for query parameter one to permit unit tests
     * @return Query parameter
     */
    public static String getQueryParam1() {
        return queryParam1;
    }
    /**
     * Public getter for query parameter two to permit unit tests
     * @return Query parameter
     */
    public static String getQueryParam2() {
        return queryParam2;
    }

}
