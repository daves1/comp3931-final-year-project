import org.junit.*;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;


/**
 * The CombinationsTest class simply checks that the returned arrayList contains the expected amount of data.
 *
 * @author Dan Aves
 */

public class CombinationsTest {

    @Test
    public void TestReturn(){
        List<Assumptions> list = new ArrayList<>();
        Combinations combinations = new Combinations();
        list = combinations.assumptionCombinations();
        int size = list.size();
        assertTrue(size == 54);
    }
}
