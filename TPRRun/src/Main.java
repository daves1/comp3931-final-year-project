import java.awt.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Main class starts the user interface, which in turn invokes layout.fxml and controller.class
 */
public class Main extends Application {

    Stage window;
    public static void main(String[] args) throws Exception {

        launch(args);
    }

    /**
     * Method used to start the JavaFx interface
     * @param primaryStage Stage for window
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/Interface/layout.fxml"));
        window = primaryStage;
        window.setTitle("Criteria Tester");

        primaryStage.setScene(new Scene(root, 1600, 600));
        window.show();
    }
}
