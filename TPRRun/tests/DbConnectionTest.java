import org.junit.*;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * This class tests that opening a connection to the database is possible
 *
 * @author Dan Aves
 */

public class DbConnectionTest {
    @Test
    public void dbConnectionTest() throws SQLException {
        new DbConnection();
        Connection con = DbConnection.openConnection();
        assertEquals(con != null, true);
    }
}
