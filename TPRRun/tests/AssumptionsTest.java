import org.junit.*;
import static org.junit.Assert.*;

/**
 * The AssumptionsTest class tests the assumption class
 *
 * @author Dan Aves
 */
public class AssumptionsTest {
    /**
     * Equals tests are used to test the robustness of the overwritten equals method
     * In each case assumption1 is the set of assumptions, whilst 'rule' are the rules testing against those assumptions
     */
    @Test
    public void equalsTest1(){
        //Assumptions testing against
        Assumptions assumptions1 = new Assumptions(null, "LN600", "Down", null, null, "SL", null, null);
        //Rule to test
        Assumptions rule = new Assumptions(null, "LN600", "Down", null, null, "SL", null, null);

        assertTrue(assumptions1.equals(rule));
    }
    @Test
    public void equalsTest2(){
        Assumptions assumptions1 = new Assumptions(null, "LN600", "Down", null, null, null, null, null);
        Assumptions rule = new Assumptions(null, "LN600", "Down", null, null, "SL", null, null);

        assertFalse(assumptions1.equals(rule));
    }
    @Test
    public void equalsTest3(){
        Assumptions assumptions1 = new Assumptions(null, "LN600", "Down", "nonstop", "stopper", "SL", null, null);
        Assumptions rule = new Assumptions(null, "LN600", "Down", "nonstop", null, "SL", null, null);

        assertTrue(assumptions1.equals(rule));
    }
    @Test
    public void equalsTest4(){
        Assumptions assumptions1 = new Assumptions(null, "LN600", "Down", "nonstop", "stopper", "SL", null, null);
        Assumptions rule = new Assumptions(null, "LN600", "Down", "nonstop", "nonstop", "SL", null, null);

        assertFalse(assumptions1.equals(rule));
    }

    /**
     * This test ensures the Getters are working correctly 
     */
    @Test
    public void createObject(){
        Assumptions assumption = new Assumptions("London", "LN600", "Up", "nonstop", "stopper", "LSL", "York", "");

        assertTrue(assumption.routeCode.equals("London"));
        assertFalse(assumption.routeCode.equals(null));
        assertTrue(assumption.lineCode.equals("LSL"));
        assertTrue(assumption.notStopAt.equals(""));
    }
}
