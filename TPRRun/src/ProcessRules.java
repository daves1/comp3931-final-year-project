import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.nio.file.OpenOption;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.*;

import javafx.scene.control.TextArea;

/**
 * Class is used to compare the timetable planning rules against the inputted assumptions
 */
public class ProcessRules {
    private static ArrayList<Geography> geographyList = null;
    private static ArrayList<Headway> headwayList = null;
    private static ArrayList<Headway> selectedRule = new ArrayList<>();
    private static List<Assumptions> assumptionsList = new ArrayList<>();

    /**
     * Stores the output to the logfile
     */
    private static String result = "";
    /**
     * Used to track the number of times ProcessRules is invoked
     */
    private static Integer prCount = 0;
    /**
     * Flag to track whether a rule is found for the current test
     */
    private static boolean ruleExists = false;
    /**
     * Updates with the applicable standard value for the given headway table
     */
    private static int standardValue = 0;
    /**
     * Stores the next timing point considered in the Geography
     */
    private static String nextTimingPoint;
    /**
     * Stores the timing point a rule is applicable until
     */
    private static String timingPointTo = "";

    /**
     * Main method for testing the rules
     * @param readAssum Defaulted to true, will read assumptions from textfile if false
     * @param prCount Represents assumption instance being tested
     * @param assumptions List containing assumptions objects
     * @param errorOnly Flag to return errors only to interface
     * @return Returns result string for interface
     */
    public static String ProcessRules(boolean readAssum, int prCount, List<Assumptions> assumptions, boolean errorOnly) {

        boolean errorFound = false;

        //Read assumptions from text file if true
        if(!readAssum) {
            readAssumptions();
        }
        else{
            assumptionsList = assumptions;
        }

        //Get data from both Geography and Headway classes and store in collection
        Headway headwayInstance = new Headway();
        Geography geographyInstance = new Geography();
        geographyList = geographyInstance.getGeographyList();
        headwayList = headwayInstance.getHeadwayList();

        result ="";
        logAssumptions();

        //Set up logging header
        result += (String.format("%-35s%-60s%-10s%-40s", "Timing Point", "Rule Applied","Value","Reason"));
        result += "\n";

        //Write header to log file
        if(prCount == 0){
            logDetails("",false);
        }
        else{
            logDetails("\n", true);
        }

        int totalGeographyTP = geographyList.size();
        //Loop through each Timing Point in the Geography for the given RouteCode and Region
        for (int i = 0; i < totalGeographyTP; i++) {
            ruleExists = false;
            //Get the current geography object from the list at index i
            Geography currentGeography = geographyList.get(i);

            //If last Timing Point then manually set nextTimingPoint to avoid out of bounds
            if(i == (totalGeographyTP - 1)){
                nextTimingPoint = "end";
            }
            else{
                Geography nextInstance = geographyList.get(i + 1);
                nextTimingPoint = nextInstance.getTimingPoint();
            }

            //Rule applied between timing points is cleared if the destination timing point is reached
            if (currentGeography.getTimingPoint().equals(timingPointTo)) {
                selectedRule.clear();
                timingPointTo = "";
            }

            //Reapply previous rule if the current timing point is not the last timing point the rule is applicable until
            if (!currentGeography.getTimingPoint().equals(timingPointTo) && timingPointTo != "") {
                Headway appliedRule = selectedRule.get(selectedRule.size() - 1);

                //Formatting for logging
                if (!errorOnly){
                    result +=  (String.format("%-35s",currentGeography.getTimingPoint()));
                    result += String.format("%-60s", appliedRule.getTimingPointFrom() + " to " + appliedRule.getTimingPointTo());
                    result += String.format("%-10s",appliedRule.getValue());
                }

            } else {
                //Consider each headway rule in the list
                for (int headwayRule = 0; headwayRule < headwayList.size(); headwayRule++) {
                    //Get the current headway object from the list at the index - headwayRule
                    Headway headwaySelection = headwayList.get(headwayRule);

                    //Does a headway rule exist for the current Timing Point
                    if (currentGeography.getTimingPoint().equals(headwaySelection.getTimingPointFrom())) {

                        //Set destination Timing Point for rule
                        timingPointTo = headwaySelection.getTimingPointTo();
                        ruleExists = true;
                        //Compare assumptions with rules
                        Compare(headwaySelection);
                    }
                    //Standard only needs setting once for the given assumptions
                    if ((headwaySelection.getTimingPointFrom().equals("Standard Headway")) && standardValue == 0) {
                        if (Objects.equals(headwaySelection.direction, assumptionsList.get(0).direction)) {
                            standardValue = headwaySelection.getValue();
                        }
                    }
                }

                //If no rule was found standard is applied
                if (!ruleExists) {
                    //Logging formatting
                    if (!errorOnly) {
                        result += (String.format("%-35s%-60s%-10s", currentGeography.getTimingPoint(), "Standard headway", standardValue));
                    }
                //Rule was found
                } else if (!selectedRule.isEmpty()) {
                    //Create headway object from the last rule added to list
                    Headway appliedRule = selectedRule.get(selectedRule.size() - 1);
                    //Logging
                    if (!errorOnly) {
                        result += (String.format("%-35s", currentGeography.getTimingPoint(), appliedRule.getTimingPointFrom(), appliedRule.getTimingPointTo(), appliedRule.getValue()));
                        result += String.format("%-60s", appliedRule.getTimingPointFrom() + " to " + appliedRule.getTimingPointTo());
                        result += String.format("%-10s", appliedRule.getValue());

                        //Get reason(s) rules applied
                        ruleReason(appliedRule);
                    }
                }
                //No rules could be applied
                else {
                    Assumptions reason = new Assumptions();
                    result +=  (String.format("%-35s%-60s%-10s%-40s",currentGeography.getTimingPoint() ,"Standard headway" , standardValue, reason.getErrorMsg()));
                    if(errorOnly){
                        result += "\n";
                        result += "to " + nextTimingPoint;
                        result += "\n\n";
                    }
                    timingPointTo = "";
                    errorFound = true;
                }
            }
            //General formatting
            if (!errorOnly){
                result += "\n";
                result += "to " + nextTimingPoint;
                result += "\n\n";
            }
        }
        //Only concerned about error message
        if (!errorFound && errorOnly){
            result = "";
        }
        logDetails(result,true);
        return result;
    }


    /**
     * Method used to compare Headway rules against current TimingPoint and Assumptions
     * @param current Headway object representing current headway to be considered
     */
    public static void Compare(Headway current){


        Assumptions assumptionsPassed = new Assumptions(assumptionsList.get(0).routeCode, assumptionsList.get(0).regionName, assumptionsList.get(0).direction, assumptionsList.get(0).following,assumptionsList.get(0).serviceType, assumptionsList.get(0).lineCode, assumptionsList.get(0).stopAt, assumptionsList.get(0).notStopAt);
        Assumptions currentRule = new Assumptions(current.routeCode, current.regionName,current.direction,current.following,current.serviceType,current.lineCode,current.stopAt,current.notStopAt);

        if(assumptionsPassed.equals(currentRule)){
            //If a standard rule is selected but a more important exception is valid then this should override the selection
            if(!Objects.equals(current.getStandard(), "y") || selectedRule.isEmpty())
            {
                selectedRule.add(current);
            }

        }
    }

    /**
     * Method used to append the reason for rule application to result string
     * @param appliedRule Headway object for the applied rule
     */
    public static void ruleReason(Headway appliedRule){

        if (appliedRule.direction != null){
            result += "Direction: " + appliedRule.direction + ", ";
        }

        if (appliedRule.serviceType != null){
            result += "Current Service Type: " + appliedRule.serviceType + ", ";
        }

        if (appliedRule.following != null){
            result += "Following: " + appliedRule.following + ", ";
        }

        if (appliedRule.lineCode != null){
            result += "Line Code: " + appliedRule.lineCode + ", ";
        }

        if (appliedRule.stopAt != null){
            result += "Stopping at:  " + appliedRule.stopAt + ", ";
        }

        if (appliedRule.notStopAt != null){
            result += "Not stopping at: " + appliedRule.notStopAt + ", ";
        }
    }

    /**
     * Calls Logging.class to output results
     * @param result String containing results
     * @param append Append log file or not
     */
    public static void logDetails(String result, Boolean append) {
        Logging logging = new Logging();

        logging.logDetails(result,append);
    }
    

    /**
     * Method used to read Assumptions from text file
     * @return List of assumptions
     */

    public static List<Assumptions> readAssumptions(){

        try{
            File file = new File("assumptions.txt");
            Scanner sc = new Scanner(file);

            ArrayList<String> assumptions = new ArrayList<String>();

            while(sc.hasNextLine()) {

                String line = sc.nextLine();
                String[] split = line.split(" ", 2);
                int length = split.length;
                if (length == 2){
                    assumptions.add(split[1]);
                }
                else{
                    assumptions.add("");
                }
            }
            assumptionsList.add(new Assumptions(assumptions.get(0),assumptions.get(1), assumptions.get(2), assumptions.get(3),assumptions.get(4),assumptions.get(5),assumptions.get(6),assumptions.get(7)));


        }catch (FileNotFoundException ex){
            System.out.println("assumptions file doesn't exist");
        }
        return assumptionsList;
    }

    /**
     * Method used to write the used Assumptions for that particular test run to the result string
     */
    public static void logAssumptions(){
        result = "Assumptions applied: \n";

        Class<?> c = CommonMethods.class;
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods)
            if (method.getName().startsWith("get")){
                try{
                    if(!method.getName().equals("getErrorMsg")){
                        String[] current = method.getName().split("get");
                        if (method.invoke(assumptionsList.get(0)).equals("")){
                            result += current[1] + " = blank, ";
                        }
                        else{
                            result += current[1] + " = " + method.invoke((assumptionsList.get(0))) + ", ";
                        }
                    }

                } catch (Exception ex){
                }
            }
        result += "\n\n";
    }
}
