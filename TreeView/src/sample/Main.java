package sample;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.stage.Stage;

/**
 * The Main class handles launching the JavaFX interface screen
 *
 * @author Dan Aves
 */
public class Main extends Application {

    Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        window = primaryStage;
        window.setTitle("Criteria Editor");

        primaryStage.setScene(new Scene(root, 1200, 600));
        window.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
