import java.util.ArrayList;

/**
 * This class represents the Headway table for a given routeCode, in the timetable planning rules
 */
public class Headway extends CommonMethods {
    /**
     * Unique identifier for rule
     */
    private int ruleId;
    /**
     * TimingPoint rule applied from
     */
    private String timingPointFrom;
    /**
     * TimingPoint rule applicable until
     */
    private String timingPointTo;
    /**
     * Flag for whether rule is a standard or not
     */
    private String standard;
    /**
     * Specific headway value for the rule
     */
    private int value;

    private static ArrayList<Headway> headwayList = new ArrayList<Headway>();

    //Default constructor
    public Headway() {

    };

    public Headway(int ruleId, String routeCode, String regionName, String timingPointFrom, String timingPointTo,String direction, String following, String serviceType, String lineCode, String stopAt, String notStopAt, int value, String standard){
        this.ruleId = ruleId;
        this.routeCode = routeCode;
        this.regionName = regionName;
        this.timingPointFrom = timingPointFrom;
        this.timingPointTo = timingPointTo;
        this.direction = direction;
        this.following = following;
        this.serviceType = serviceType;
        this.lineCode = lineCode;
        this.stopAt = stopAt;
        this.notStopAt = notStopAt;
        this.value = value;
        this.standard = standard;
    }

    public int getRuleId() {
        return ruleId;
    }

    public String getTimingPointFrom() {
        return timingPointFrom;
    }

    public String getTimingPointTo() {
        return timingPointTo;
    }

    public int getValue() {
        return value;
    }

    public String getStandard() {return standard;}

    public static ArrayList<Headway> getHeadwayList() {
        return headwayList;
    }

    public static void setHeadwayList(ArrayList<Headway> headwayList) {
        Headway.headwayList = headwayList;
    }
}
