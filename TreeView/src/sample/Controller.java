package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import java.util.*;

/**
 * The Controller class creates a JavaFX interface and populates it with a TreeView based on some outset critiera.
 *
 * @author Dan Aves
 */

public class Controller {

    @FXML
    private StackPane mainStackPane;
    @FXML
    private TextField selectedItemText;
    @FXML
    private TextArea logText;


    Alert a = new Alert(Alert.AlertType.NONE);

    LinkedHashMap<String,Integer> treeIndex = new LinkedHashMap<>();

    TreeView treeView = new TreeView();
    String root;
    TreeItem rootItem = new TreeItem("Route Code");

    //Variable declaration
    int indexCount = 0;
    boolean noChildren = false;

    //Create instance of Logging class
     Logging logger = new Logging();


    public void initialize() {
        setupTree();
    }

    /**
     * This fetches the content of the treeview and starts up the interface
     */
    public void setupTree() {


        //Create instance of TreeViewContent
        TreeViewContent instance = new TreeViewContent();

        //Store criteria in arraylist
        ArrayList<TreeItem> criteria = instance.getCriteria("criteria.txt");

        //Tree is editable
        treeView.setEditable(true);

        treeView.getSelectionModel().selectFirst();

        rootItem.setExpanded(true);

        //Add all contents
        rootItem.getChildren().addAll(criteria);
        treeView.setRoot(rootItem);

        root = rootItem.getValue().toString();

        treeView.setCellFactory(TextFieldTreeCell.forTreeView());

        //Event handlers which detect changes on the interface
        treeView.setOnEditStart(new EventHandler<TreeView.EditEvent>() {
            @Override
            public void handle(TreeView.EditEvent event) {
                startEdit(event);
            }
        });

        treeView.setOnEditCommit(new EventHandler<TreeView.EditEvent>()
        {
            @Override
            public void handle(TreeView.EditEvent event) {
                editSubmitted(event);
            }
        });
        treeView.setOnEditCancel(new EventHandler<TreeView.EditEvent>()
        {
            @Override
            public void handle(TreeView.EditEvent event) {
                editCancelled(event);
            }
        });

        //Add tree to interface
        mainStackPane.getChildren().add(treeView);
    }

    /**
     * This logs to the screen when a user starts to edit a particular branch
     * @Param TreeView Edit event
     */
    private void startEdit(TreeView.EditEvent event){
        logText.appendText("[Started editing] - '" + event.getTreeItem() + "\n" );
    }
    /**
     * This logs to the screen the old value of a node when it is successfully changed
     * @Param TreeView Edit event
     */
    private void editSubmitted(TreeView.EditEvent event ){
        logText.appendText("[Node changed] old - '" + event.getOldValue() + "'- new - " + event.getNewValue() + "\n" );
    }
    /**
     * This logs to the screen if editing of an node starts but is then cancelled
     * @Param TreeView Edit event
     */
    private void editCancelled(TreeView.EditEvent event){
        logText.appendText("[Cancelled editing] - '" + event.getTreeItem() + "\n" );
    }

    /**
     *  Handles a user clicking 'Add Node' on the interface
     */
    public void addItemClicked(){
        TreeItem parent = (TreeItem) treeView.getSelectionModel().getSelectedItem();

        if(parent == null){
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("A node must be selected to add to");
            a.show();
        }
        else if (!selectedItemText.getText().trim().isEmpty())
        {
            TreeItem newItem = new TreeItem(selectedItemText.getText());
            parent.getChildren().add(newItem);
            logText.appendText("[Added node] - '" + selectedItemText.getText() + "' - to - " + treeView.getSelectionModel().getSelectedItem() + "\n" );

        }
        else{
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("Please center criteria into the textfield");

            a.show();
        }
    }

    /**
     *  Handles a user clicking 'Remove Node' on the interface
     */
    public void removeItemClicked(){

        TreeItem item = (TreeItem) treeView.getSelectionModel().getSelectedItem();

        if (item != null){
            TreeItem parent = item.getParent();
            logText.appendText("[Removed node] - '" +  treeView.getSelectionModel().getSelectedItem()  + "' - from - " + parent.getValue() + "\n" );
            parent.getChildren().remove(item);

        }
        else {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setContentText("Please select a node to remove");
            a.show();
        }

    }

    /**
     *  Handles a user clicking 'Save changes' on the interface
     *  Calls out to separate class to saves changes to a text file
     */
    public void saveChangesClicked(){
        treeIndex.clear();
        saveTreeState(rootItem);
        OutputTree instance = new OutputTree();
        logger.logDetails(logText.getText());
        if (instance.outputTree(treeIndex) == true){
            logText.appendText("[Saved Criteria successfully]"+ "\n" );
        }
        else{
            logText.appendText("[Criteria saved unsuccessfully]"+ "\n" );
        }


    }

    /**
     *  At the time of call this saves the current state of the entire tree
     * @Param TreeItem - Root Item of tree
     */
    private void saveTreeState(TreeItem<String> rootItem){
        for(TreeItem<String> child: rootItem.getChildren()){
            //Check that the last branch finished and next item isn't root
            if(noChildren & rootItem.getValue() != root){
                //Get the new index from the parent
                indexCount = treeIndex.get(child.getParent().getValue()) + 1;
                noChildren = false;
            }
            else if (rootItem.getValue() == root){
                indexCount = 0;
            }
            if(child.getChildren().isEmpty()  & rootItem.getValue() != root){
                //Add 1 as parent is one behind desired index
                indexCount = treeIndex.get(child.getParent().getValue()) + 1;
                //Allows next criteria index to be fetched from parent
                noChildren = true;
                treeIndex.put(child.getValue(), indexCount);
            }
            else{
                treeIndex.put(child.getValue(), indexCount);
                indexCount++;
                saveTreeState(child);
            }
        }
    }
}
