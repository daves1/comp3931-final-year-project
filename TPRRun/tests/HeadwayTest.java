import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * The HeadwayTest class tests the Geography class
 *
 * @author Dan Aves
 */
public class HeadwayTest {
    /**
     * Tests constructor and relevant getters
     */
    @Test
    public void createObject(){
        Headway headway = new Headway(1 , "LN700","London North Eastern", "From Test", "To Test", "Down", "stopper","stopper", "FL", "York", "", 240, "y") ;

        assertTrue(headway.getTimingPointTo().equals("To Test"));
        assertTrue(headway.getValue() == 240);
    }

    @Test
    public void testHeadwayList(){
        ArrayList<Headway> headwayCol = new ArrayList<Headway>();
        ArrayList<Headway> getHeadwayCol = new ArrayList<Headway>();
        Headway headway = new Headway(1 , "LN600","London North Eastern", "From Test", "To Test", "Down", "stopper","stopper", "FL", "York", "", 240, "y");
        Headway headway2 = new Headway(1 , "LN700","London North Eastern", "From Test", "To Test", "Down", "stopper","stopper", "FL", "York", "", 240, "y");

        headwayCol.add(headway);
        headwayCol.add(headway2);

        Headway newInstance = new Headway();
        newInstance.setHeadwayList(headwayCol);

        getHeadwayCol = newInstance.getHeadwayList();
        assertTrue(getHeadwayCol.get(0).routeCode.equals("LN600"));
        assertTrue(getHeadwayCol.get(1).routeCode.equals("LN700"));
    }
}
