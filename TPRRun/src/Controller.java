import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.control.CheckBox;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for Controller.java. Used to communicate with the interface and start the
 * respective backend operations.
 *
 * @author Dan Aves
 */

public class Controller {

    /**
     * Fields used to collect the respective user input from the interfaces TextFields.
     */
    @FXML
    public TextField routeCodeField, regionField, directionField, serviceType1, lineCode1, trainStop1, trainNoStop1, serviceType2;
    /**
     * Handle optional checkbox input from interface
     */
    @FXML
    public CheckBox testAllCheckBox, onlyErrorCheckBox;
    /**
     * Area to output results to screen
     */
    @FXML
    public TextArea screenResults;

    Alert a = new Alert(Alert.AlertType.NONE);

    private String errorReason;

    /**
     * Handles a user click on the 'run' button
     */
    public void runButtonClick(){

        boolean pass = checkInput();

        if (pass){
            displayError();
        }
        else{
            runTest();
        }
    }

    /**
     * Validates the user input
     * @return Whether there is an error
     */
    public boolean checkInput(){
        boolean error = false;
        if (routeCodeField.getText().isEmpty() || regionField.getText().isEmpty() || directionField.getText().isEmpty() ){
            errorReason = "Either RouteCode, Region or Direction cannot be empty";
            error = true;

        }
        else if (!routeCodeField.getText().toUpperCase().equals("LN600")){
            errorReason = "Only route code LN600 is currently in the database";
            error = true;
        }
        else if (!regionField.getText().equals("London North Eastern")){
            errorReason = "Only region London North Eastern is currently in the database";
            error = true;
        }
        else if (!directionField.getText().toUpperCase().equals("UP") && !directionField.getText().toUpperCase().equals("DOWN")){
            errorReason = "Direction can only be Up or Down";
            error = true;
        }
        return error;
    }

    /**
     * Responsible for populating Assumptions.class and invoking ProcessRules.class
     */
    public void runTest(){
        //Clear screen for each run
        screenResults.clear();

        List<Assumptions> assumptionsList = new ArrayList<>();

        boolean allAssumptions = testAllCheckBox.isSelected();

        //Fetch assumptionsList of every combination
        if(allAssumptions){
            Combinations combinations = new Combinations();
            assumptionsList = combinations.assumptionCombinations();
        }
        //Otherwise create assumptions list based on user input
        else{
            assumptionsList.add(new Assumptions(routeCodeField.getText().toUpperCase(), regionField.getText(), directionField.getText().toUpperCase(),serviceType2.getText().toUpperCase(), serviceType1.getText().toUpperCase() ,lineCode1.getText().toUpperCase(), trainStop1.getText().toUpperCase(), trainNoStop1.getText().toUpperCase()));
        }

        //Populate objects with data from database
        try{
            FetchData fetchData = new FetchData();
            fetchData.createCollection(assumptionsList.get(0).routeCode, assumptionsList.get(0).regionName);
        }catch (SQLException ex){
            System.out.println("Failed to obtain data from FetchData.class " + ex);
        }

        //Create instance
        ProcessRules processRules = new ProcessRules();

        //Call ProcessRules for each assumption in list
        for (int assumptionCount =0 ; assumptionCount < assumptionsList.size(); assumptionCount++){
            //Populate assumptions object with current assumptions
            List<Assumptions> currentAssumptions = new ArrayList<>();
            currentAssumptions.add(assumptionsList.get(assumptionCount));

            String result = processRules.ProcessRules(true, assumptionCount, currentAssumptions, onlyErrorCheckBox.isSelected());

            //Append returned result to screen
            screenResults.appendText(result);

        }
    }

    /**
     * Creates on screen prompt for errors
     */
    public void displayError(){
        a.setAlertType(Alert.AlertType.INFORMATION);
        a.setContentText(errorReason);
        a.show();

    }

}
