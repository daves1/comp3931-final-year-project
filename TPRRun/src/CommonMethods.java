/**
 * Used to declare fields which make up the basis of the Timetable Planning Rules
 * @author Dan Aves
 */
public class CommonMethods {

    /**
     * Route specific code for the region
     */
    protected String routeCode;
    /**
     * Name of the region
     */
    protected String regionName;
    /**
     * The direction of travel for the train
     */
    protected String direction;
    /**
     * What service the current service is following
     */
    protected String following;
    /**
     * The current service type
     */
    protected String serviceType;
    /**
     * Specific code which dictates the type of line
     */
    protected String lineCode;
    /**
     * Station train is stopping at
     */
    protected String stopAt;
    /**
     * Station train is not stopping at
     */
    protected String notStopAt;



    //Default constructor
    public CommonMethods() {

    };

    public String getRouteCode() {
        return routeCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getDirection() {
        return direction;
    }

    public String getFollowing() {
        return following;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getLineCode() {
        return lineCode;
    }

    public String getStopAt() {
        return stopAt;
    }

    public String getNotStopAt() {
        return notStopAt;
    }

}
