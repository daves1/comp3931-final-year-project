import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * This class is used to log the results from ProcessRules.class
 */
public class Logging {

    /**
     * Method outputs the passed data to rulesApplied.log text file
     * @param result string containing the results
     * @param append boolean to dictate whether file should be appended or refreshed
     */
    public static void logDetails(String result, Boolean append){

        Logger logger = Logger.getLogger("rulesLog");
        logger.setUseParentHandlers(false);
        try{
            FileWriter handler = new FileWriter("rulesApplied.log", append);

            PrintWriter pw = new PrintWriter(handler);

            pw.println(result);
            pw.close();
            handler.close();

        }catch (IOException ex){
            System.out.println(ex);
        }
    }
}
