import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * The GeographyTest class tests the Geography class
 *
 * @author Dan Aves
 */
public class GeographyTest {

    /**
     * Tests constructor and relevant getters
     */
    @Test
    public void createObject(){
        Geography geography = new Geography(1, "LN600", "London North Eastern","Mock Timing Point","Down","Up","SL","Headway","Testing Notes");
        assertTrue(geography.regionName.equals("London North Eastern"));
        assertFalse(geography.routeCode.equals(null));
        assertTrue(geography.getTimingPoint().equals("Mock Timing Point"));
        assertTrue(geography.getNotes().equals("Testing Notes"));
    }
}
