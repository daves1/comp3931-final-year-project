import java.lang.reflect.Method;
import java.util.Objects;


/**
 * The assumptions class is used to create a representation of route specific train timetable information.
 * The fields are populated with either user input or multi-case automated testing.
 *
 * @author Dan Aves
 */
public class Assumptions extends CommonMethods {

    /**
     * Error message describing the reason a rule wasn't applied.
     */
    static String errorMsg;


    //Default constructor

    public Assumptions() {
    };

    public Assumptions(String routeCode, String regionName, String direction, String following, String serviceType, String lineCode, String stopAt, String notStopAt){
        this.routeCode = routeCode;
        this.regionName = regionName;
        this.direction = direction;
        this.following = following;
        this.serviceType = serviceType;
        this.lineCode = lineCode;
        this.stopAt = stopAt;
        this.notStopAt = notStopAt;
    }

    public void setRouteCode(String junctionName) {
        this.routeCode = routeCode;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public static String getErrorMsg() { return errorMsg; }



    /**
     * @param obj
     * This method overrides the equals method when comparing two Assumption objects.
     * Loops through each getter in Assumptions and returns false when:
     *  - the current headway rule is not null
     *  - the current headway rule doesn't equal the assumption
     */
    @Override
    public boolean equals(Object obj){
        //Flag used to determine whether rule is found
        boolean result = true;

        Class<?> c = CommonMethods.class;
        Method[] methods = c.getDeclaredMethods();
        //For each 'get' method in Common Methods
        for (Method method : methods)
            if (method.getName().startsWith("get")){
                try{
                    //obj = current headway rule , this = assumptions
                    //Current headway rule not null and the rules do not equal assumption criteria
                    if(!Objects.equals(method.invoke(obj), null) && !Objects.equals(method.invoke(obj), method.invoke(this))){
                        result = false;
                        if(!method.getName().equals("getDirection")) {
                            String[] reason = method.getName().split("get");
                            //Rule couldn't be placed, append error message
                            if (method.invoke(this).equals("")){
                                errorMsg = "No rule applied, rule(s) required a different " + reason[1] + " to the assumptions - blank";
                            }
                            else{
                                errorMsg = "No rule applied, rule(s) required a different " + reason[1] + " to the assumptions - " + method.invoke(this);
                            }

                        }
                    }
                } catch (Exception ex){
                }
            }
        return result;
    }

}
