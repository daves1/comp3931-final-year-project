import javafx.scene.control.TreeItem;
import org.junit.*;
import sample.TreeViewContent;
import java.io.FileNotFoundException;
import java.util.ArrayList;


import static org.junit.Assert.*;


/**
 * The class ContentTest is used perform tests against TreeViewContent.class
 */
public class ContentTest {

    /**
     * This method is testing that invalid files are handled
     */
    @Test
    public void exceptionTest(){
        TreeViewContent instance = new TreeViewContent();

        Exception ex = assertThrows(FileNotFoundException.class, () -> {
            instance.ReadInput("nosuchfile.txt");
        });

    }

    /**
     * Ensures that branches are represented correctly from a mock datafile
     */
    @Test
    public void testDataStructure() {
        TreeViewContent instance = new TreeViewContent();

        ArrayList<TreeItem> criteria = instance.getCriteria("tests/testTree.txt");

        assertEquals(criteria.get(0).getValue(),"TestBranch1");
        TreeItem child = (TreeItem) criteria.get(0).getChildren().get(0);
        TreeItem child2 = (TreeItem) child.getChildren().get(0);
        assertEquals(child.getValue(),"Child1");
        assertEquals(child2.getValue(), "Child2");
    }

    /**
     * Tests multiple branches
     */
    @Test
    public void testMultipleBranches(){
        TreeViewContent instance = new TreeViewContent();

        ArrayList<TreeItem> criteria = instance.getCriteria("tests/testTree.txt");

        assertEquals(criteria.get(1).getValue(),"TestBranch2");
    }





}
