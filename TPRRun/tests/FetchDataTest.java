import org.junit.*;
import static org.junit.Assert.*;
import java.sql.PreparedStatement;


/**
 * The FetchDataTest class checks to ensure the parameters passed to createQueries() are retrieved correctly.
 *
 * @author Dan Aves
 */
public class FetchDataTest {

    private static PreparedStatement geographyTest = null;
    private static PreparedStatement headwayStatement = null;

    @Test
    public void PreparedStatementTest(){
        Assumptions assumptionsIn = new Assumptions();
        assumptionsIn.setRouteCode("LN500");
        assumptionsIn.setRegionName("London North Eastern");

        FetchData fetchData = new FetchData();
        fetchData.createQueries("LN500", "London North Eastern");

        assertTrue(fetchData.getQueryParam1().equals(("LN500")));
        assertTrue(fetchData.getQueryParam2().equals(("London North Eastern")));



    }


}
