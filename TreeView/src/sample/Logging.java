package sample;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

/**
 * The Logging class outputs the tree editor changes into a log file
 *
 * @author Dan Aves
 */
public class Logging {
    /**
     * Outputs the passed string to the log file
     * @param result String containing results to append to log file 
     */
    public static void logDetails(String result) {
        //boolean append = false;
        Logger logger = Logger.getLogger("");
        logger.setUseParentHandlers(false);
        try {
            FileWriter handler = new FileWriter("log.txt", true);

            PrintWriter pw = new PrintWriter(handler);

            pw.println(result);
            pw.close();
            handler.close();



        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
