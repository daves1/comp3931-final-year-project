import java.sql.*;

/**
 * Class responsible for opening a database connection
 */
public class DbConnection {

    /**
     * Tries to open a connection to rules.db
     * @return the open connection
     */
    public static Connection openConnection(){
        Connection connection = null;
        //Database location
        String database = "../rules.db";

        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:"+ database);

        }
        catch(SQLException e) {
            System.err.println("Connection failed");
            System.err.println(e.getMessage());
        }
        return connection;
    }
}
